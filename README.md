## stm32f0_bmsk - A Bare Metal Skelethon for STM32F0xx microcontroller
A base fw, just a skelethon with some low level "drivers", for STM32F0
family microcotrollers.

It's not complete as CMSIS could be but is a starting point from which
one can rapidly develop complex, essential and clean firmware.

It's fast to understand and need data-sheet and user manual at hand.

Tested with STM32F042 but easy to adapt to other microcotroller of the
same family.

## quick start

Use arm-none-eabi- toolchain and other standard build tool available
on every linux distro (find, grep, sed, make and so on...).

Just type

    $ make
	
compiled code will be in build/

To program the microcotroller use standard downloader

    $ stm32flash build/stm32f042_base.hex /dev/ttyUSB0
	
or whatever the interface used.

## adapt the code

TODO
