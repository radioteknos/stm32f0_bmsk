/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#define __HW_VERSION "1.0"
#define __FW_VERSION "1.0"


#define BUFFLEN 32

/* stat bits */
#define hbledD          31  /* ! don't move ! */
#define hbledC          30  /* ! don't move ! */
#define hbledB          29  /* ! don't move ! */
#define hbledA          28  /* ! don't move ! */

#define ee_fault         1
#define debugmode        0

/* heartbeat led mode */
#define HBPATT_NORMAL     0
#define HBPATT_ALARM      1
#define HBPATT_INVALARM   2
#define HBPATT_OFF        3
#define HBPATT_ON         4
#define HBPATT_CALADJ     5
#define HBPATT_UNCAL      6
#define HBPATT_TBD3       7
#define HBPATT_INVNORMAL  8
#define HBPATT_TBI2       9
#define HBPATT_TBI3      10
#define HBPATT_TBI4      11
#define HBPATT_TBI5      12
#define HBPATT_TBI6      13
#define HBPATT_TBI7      14
#define HBPATT_TBI8      15

#define HBLED_PORT GPIOA
#define HBLED_PIN 4

