/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

/* 7bit device addr */
#define EEADDR        0x50  /* 0xa0 for 24LCxx */


#define ee_hw_major_ver   0x00         /* hw major version number - uint8_t */

/* #define ee_ 0  /\*  *\/ */
#define ee_lastunused     0x01

uint16_t ext_eeprom_wb(uint16_t addr, uint8_t v),
  ext_eeprom_rb(uint16_t addr, uint8_t* v);

void ee_save(void *var, uint8_t len, uint16_t ee_base_addr),
  ee_load(void *var, uint8_t len, uint16_t ee_base_addr),
    eeprom_default_check_and_fill(uint8_t force);
