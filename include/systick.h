/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#define PRESCTASKCNTVAL      10  /* prescaled tasks are executed every 10ms */
#define HEARTBEAT_PRESC_VAL 100  /* in systicks (1ms) unit*/

#define hbled_on() HBLED_PORT->BSRR=(1<<HBLED_PIN);
#define hbled_off() HBLED_PORT->BSRR=(1<<(HBLED_PIN+16));

void hbledmode_set(uint8_t mode);

void delay_ms(uint32_t); 
uint32_t systick_init(uint32_t ticks);
void systick_isr(void);
