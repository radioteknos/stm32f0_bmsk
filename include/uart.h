/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#define RXBUFFLEN 60

/* usart1stat bits */
#define uartprompt  16

#define txempty      8
#define rxerror      7
#define rxbreak      6
#define rxfrmerr     5
#define rxovrerr     4
#define rxnoiseerr   3
#define rxparerr     2
#define rxoverbuff   1
#define uartecho     0

void usart1_init(uint32_t baudrate), usart1_isr(void), usart1_putc(char c),
  usart1_puts(char* b);
int usart1_get_next_msg(char *b);

#define usart1_send_error() usart1_puts("\a\r\n");
#define usart1_send_crlf() usart1_puts("\r\n");
#define usart1_send_prompt() usart1_puts("\r\n[--:--:--] >");
