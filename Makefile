#
#  stm32f0_bmsk
#
#     (c) by Lapo Pieri 2021
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Send bugs reports, comments, critique, etc, to
#
#       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
#

# general settings
PROJECT_NAME  := stm32f0_bmsk
ARCH_FLAGS := -mthumb -mcpu=cortex-m0 -march=armv6-m # cortex M0
BUILD_DIR := build
SOURCE_DIR := src
INCLUDE_DIR := include
CORE_DIR := core
CFLAGS	= -W -Wall -Wextra -Wshadow -std=gnu99 -ffunction-sections -fdata-sections $(ARCH_FLAGS) -O3 -g -D M24LC256
DBGFLAGS =
ASFLAGS	= -W $(ARCH_FLAGS)

LDFLAGS = $(LINKER_FLAGS) -Wextra $(ARCH_FLAGS) -Wl,--no-wchar-size-warning
# display all warnings; compile functions and data into their own sections so
# they can be discarded if unused; the linker performs garbage collection of
# unused input sections
LINKERSCRIPTS := $(CORE_DIR)/stm32f042f6p.ld
LINKER_FLAGS := --specs=nano.specs --specs=rdimon.specs -lc -lc -lrdimon -Wl,--gc-sections

# tools
CAT	:= cat
ECHO	:= echo
FIND	:= find
GREP	:= grep
MKDIR	:= mkdir -p
RM	:= rm -r
SED	:= sed
SHUF	:= shuf

TARGET  := arm-none-eabi
AS	:= $(TARGET)-as
CC	:= $(TARGET)-gcc
CXX	:= $(TARGET)-g++
OBJCOPY	:= $(TARGET)-objcopy
SIZE	:= $(TARGET)-size


# components
SOURCES += src/main.c src/systick.c src/uart.c src/misc.c src/callback.c
SOURCES += src/i2c.c src/eeprom.c src/adc.c src/timer.c
SOURCES += $(CORE_DIR)/startup_stm32f0xx.c $(CORE_DIR)/system_stm32f0xx.c
INCLUDES := $(INCLUDE_DIR) $(CORE_DIR) #$(CORE_DIR)/cortex-m drivers/inc

# default target
all: $(BUILD_DIR)/$(PROJECT_NAME).elf

# rules

# translate this list of sources into a list of required objects
# in the build directory
objects = $(patsubst %.c,%.o,$(patsubst %.S,%.o,$(SOURCES)))
OBJECTS = $(addprefix $(BUILD_DIR)/,$(objects))

# rule for generating object and dependancy files from source files
#
# creates a directory in the build tree if nessesary; file is only compiled,
# not linked; dependancy generation is automatic, but only for user header
# files; every depandancy in the .d is appended to the .d as a target, so that
# if they stop existing the corresponding object file will be re-compiled
$(BUILD_DIR)/%.o: %.c
	@$(MKDIR) $(BUILD_DIR)/$(dir $<)
	$(CC) -c -MMD $(CFLAGS) $(DBGFLAGS) $(addprefix -I,$(INCLUDES)) -o $@ $<
	@$(SED) -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' -e '/^$$/ d' -e 's/$$/ :/' < $(BUILD_DIR)/$*.d >> $(BUILD_DIR)/$*.d;

# attempt to include the dependany makefiles for every object in this makefile
# This means that object files depend on the header files they include
-include $(OBJECTS:.o=.d)

# rule for generating object files from assembler files
# creates a directory in the output tree if nessesary; the file is only
# assembled, not linked
$(BUILD_DIR)/%.o: %.s
	@$(MKDIR) $(BUILD_DIR)/$(dir $<)
#	$(AS) $(ASFLAGS) -o $@ $<
	$(CC) $(ASFLAGS) -c -o $@ $<

# generate the main build artifact
# a .elf containing all the symbols (i.e. debugging information if the compiler
# / linker was run with -g) is created, alongside .hex and .bin files; A just
# about human-readable .map is also created
$(BUILD_DIR)/$(PROJECT_NAME).elf: $(OBJECTS) $(LINKERSCRIPTS)
#	$(CC) $(LDFLAGS) $(DBGFLAGS) $(addprefix -T,$(LINKERSCRIPTS)) -nostartfiles -Wl,-Map,$(@:.elf=.map) -o $@ $(OBJECTS)
	$(CC) $(LDFLAGS) $(DBGFLAGS) $(addprefix -T,$(LINKERSCRIPTS))  -Wl,-Map,$(@:.elf=.map) -o $@ $(OBJECTS)
	@$(OBJCOPY) -O binary $@ $(@:.elf=.bin)
	@$(OBJCOPY) -O ihex $@ $(@:.elf=.hex)
	$(SIZE) $@
	@$(SIZE) $@|tail -1 -|awk '{print "used: ROM "($$1+$$2)" \t RAM "($$2+$$3)""}'

# removes everything in the output directory
.PHONY: clean
clean:
	$(RM) -f $(BUILD_DIR)/*
	$(RM) -f $(SOURCE_DIR)/*~
	$(RM) -f $(INCLUDE_DIR)/*~
	$(RM) -f $(CORE_DIR)/*~
	$(RM) -f *~

size:
	$(SIZE) $(BUILD_DIR)/$(PROJECT_NAME).elf
	@$(SIZE) $(BUILD_DIR)/$(PROJECT_NAME).elf|tail -1 -|awk '{print "used: ROM "($$1+$$2)" \t RAM "($$2+$$3)""}'
