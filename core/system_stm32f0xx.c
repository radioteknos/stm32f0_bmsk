#include "stm32f0xx.h"

uint32_t sys_clk;


static void sys_clk_set(void);

void system_init(void){
  
  /* Set HSION bit */
  RCC->CR|=0x00000001;

  /* rst SW, HPRE, PPRE, ADCPRE, MCOSEL, MCOPRE, PLLNODIV */
  RCC->CFGR&=0x08FFB80C; /* (use 0xF8FFB80C for STM32F051) */
  
  /* rst HSEON, CSSON, PLLON */
  RCC->CR&=0xFEF6FFFF;

  /* rst HSEBYP */
  RCC->CR&=0xFFFBFFFF;

  /* rst PLLSRC, PLLXTPRE, PLLMUL */
  RCC->CFGR&=0xFFC0FFFF;

  /* rst PREDIV1 */
  RCC->CFGR2&=0xFFFFFFF0;

  /* rst USARTSW, I2CSW, CECSW, ADCSW */
  RCC->CFGR3&=0xFFFFFEAC;

  /* rst HSI14 */
  RCC->CR2&=0xFFFFFFFE;

  /* disable ints */
  RCC->CIR=0x00000000;

  /* clk cfg (sysclk, ahb presc, apb presc */
  sys_clk_set();
}


/* internal 48MHz clock (HSI) select as SYSCLK */
static void sys_clk_set(void){

  /* start with HSI (8MHz) (really usefull?) */
  RCC->CR|=RCC_CR_HSION;

  /* HCLK = SYSCLK */
  RCC->CFGR|=RCC_CFGR_HPRE_DIV1;
  
  /* PCLK = HCLK */
  RCC->CFGR|=RCC_CFGR_PPRE_DIV1;

  /* Select HSI48 as system clock source */
  RCC->CR2|=RCC_CR2_HSI48ON;
  while(!(RCC->CR2&RCC_CR2_HSI48RDY)){
    /* TODO: add timeout  */
  }
  RCC->CFGR&=~RCC_CFGR_SW;
  RCC->CFGR|=RCC_CFGR_SW_HSI48;

  sys_clk=48000000;
}
