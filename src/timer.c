/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include <string.h>
#include "stm32f0xx.h"
#include "main.h"
#include "timer.h"

extern uint32_t sys_clk;
uint32_t tim3_evnt=0;

void tim3_init(uint16_t presc, uint32_t rate){

  /* stop the timer */
  if(presc==0 || rate==0){
    /* stop tim3 */
    TIM3->CR1&=~(TIM_CR1_CEN);
    /* clear pending irq */
    TIM3->SR&=~(TIM_SR_UIF);
  }
  /* init and start the timer */
  else{
    /* power up tim3 and make sure it's disabled */
    RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
    TIM3->CR1&=~TIM_CR1_CEN;
    
    /* reset tim3 */
    RCC->APB1RSTR |=  (RCC_APB1RSTR_TIM3RST);
    RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM3RST);
    
    /* preset and reload settings */
    TIM3->PSC=presc;
    TIM3->ARR=rate;
    
    /* update tim3 regs */
    TIM3->EGR|=TIM_EGR_UG;
    
    /* cnfigure NVIC for tim3 irq */
    NVIC_EnableIRQ(TIM3_IRQn);
    NVIC_SetPriority(TIM3_IRQn, 5);  /* TODO: adj pri */
    
    /* enable irq */
    TIM3->DIER|=TIM_DIER_UIE;
    
    /* enable TIM3 */
    TIM3->CR1=TIM_CR1_CEN;
  }
}


void tim3_isr(void){
  /* ack irq */
  TIM3->SR&=~(TIM_SR_UIF);

  tim3_evnt++; /* (?) TODO: remove (?) */

  /* usefull code here */
}

void pwm_tim14_init(void){

  /* power up tim14 */
  RCC->APB1ENR|=RCC_APB1ENR_TIM14EN;

  /* GPIOB AF set*/
  /* TMR14_CH1 (AF0)  */
  GPIOB->MODER&=~(0x3<<(2));  /* AF */
  GPIOB->MODER|=(0x2<<(2));
  GPIOB->AFR[0]&=~0xf0;       /* AF0 */
  GPIOB->AFR[0]|=0x00;

  
  TIM14->PSC=47;      /* x48 prescaler */
  TIM14->ARR=100;     /* count up to 100, so 100us period (10kHz) */
  TIM14->CCR1=0;      /* start with 0% pwm */
  TIM14->CCMR1=0x60;  /* pwm mode 1 */
  TIM14->CCER=0x1;    /* enable OC1 output */
  TIM14->EGR=0x1;     /* update all register */
  TIM14->CR1=0x1;     /* enable TIM14 */
  /* TIM14->EGR=0x1; /\* ??? TODO: try to remove *\/ */

}
