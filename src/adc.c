/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include <string.h>
#include "stm32f0xx.h"
#include "main.h"
#include "adc.h"

void adc_init(void){

  /* define ADC_IN pin(s) */
  /* ADC_IN[0] is PA0 */
  GPIOA->MODER|=(0x3<<(0*2));


  /* power up adc */
  RCC->APB2ENR|=RCC_APB2ENR_ADC1EN;

  /* select adc clock */
  ADC1->CFGR2=ADC_CFGR2_CKMODE_1 | ADC_CFGR2_CKMODE_0; /* use PCLK/2
						       (i.e. 12MHZ w/ HSI48) */

  /* adc calib (use funct param to en/dis) */
  ADC1->CR=0x0;
  ADC1->CR=ADC_CR_ADCAL;
  while((ADC1->CR&ADC_CR_ADCAL)==ADC_CR_ADCAL); /* TODO: add timeout? */

  ADC1->CFGR1=0x00000000; /* single conversion, sw SOC */
  ADC1->SMPR=0x111; /* 239.5 adc clk cycles (???) */
  ADC->CCR=ADC_CCR_VREFEN | ADC_CCR_TSEN; /* enable int vref source 
					     and int temp sensor */
  /* en adc */
  ADC1->CR=ADC_CR_ADEN;
  
}

uint16_t adc_get(uint8_t ch){
  uint16_t v;

  if((ADC1->CR&ADC_CR_ADEN)==0)
    return 0xffff;
  if(ch>18)
    return 0xfffe;
  
  /* check for ADSTART=0 */
  while((ADC1->CR&ADC_CR_ADSTART)==ADC_CR_ADSTART); /* TODO: add timeout? */
  
  ADC1->CHSELR=(1<<ch);

  /* SOC */
  ADC1->CR|=ADC_CR_ADSTART;
  
  /* wait for EOC */		   
  while((ADC1->ISR&ADC_ISR_EOC)==0); /* TODO: add timeout */
    
  v=ADC1->DR;

  return v;
}
  
