/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include "stm32f0xx.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "uart.h"
#include "i2c.h"
#include "systick.h"
#include "misc.h"
#include "eeprom.h"

extern volatile uint32_t stat;


uint16_t ext_eeprom_wb(uint16_t addr, uint8_t v){
#ifdef M24LC08
  uint8_t block;
#endif

#if defined M24LC256
  I2C1->CR2=(3<<16) | (EEADDR<<1);
#elif defined M24L08
  block=(addr>>8)&0x3;
  I2C1->CR2=(2<<16) | ((EEADDR|block)<<1);
#else
#error "define eeprom type"
#endif
  
  I2C1->CR2|=I2C_CR2_START;
  while(!((I2C1->ISR&I2C_ISR_TXE)==(I2C_ISR_TXE))); /* TODO: add timeout? */
  
#if defined M24LC256
  I2C1->TXDR=(addr>>8);
  while(!((I2C1->ISR&I2C_ISR_TXE)==(I2C_ISR_TXE))); /* TODO: add timeout? */
#endif
  
  I2C1->TXDR=(addr&0xff);

  while(!((I2C1->ISR&I2C_ISR_TXE)==(I2C_ISR_TXE))); /* TODO: add timeout? */

  I2C1->TXDR=v;
  
  while(!((I2C1->ISR&I2C_ISR_TC)==(I2C_ISR_TC))); /* TODO: add timeout? */

  I2C1->CR2|=I2C_CR2_STOP;
      
  return 0; /* TODO: add error checking otherwise locks */
}

uint16_t ext_eeprom_rb(uint16_t addr, uint8_t* v){
#ifdef M24LC08
  uint8_t block;
#endif

#if defined M24LC256
  I2C1->CR2=(2<<16) | (EEADDR<<1);
#elif defined M24L08
  block=(addr>>8)&0x3;
  I2C1->CR2=(1<<16) | ((EEADDR|block)<<1);
#else
#error "define eeprom type"
#endif

  I2C1->CR2|=I2C_CR2_START;
  while(!((I2C1->ISR&I2C_ISR_TXE)==(I2C_ISR_TXE)));  /* TODO: add timeout? */

#if defined M24LC256
  I2C1->TXDR=(addr>>8);
  while(!((I2C1->ISR&I2C_ISR_TXE)==(I2C_ISR_TXE))); /* TODO: add timeout? */
#endif

  I2C1->TXDR=(addr&0xff);
  while(!((I2C1->ISR&I2C_ISR_TC)==(I2C_ISR_TC))); /* TODO: add timeout? */

    
#if defined M24LC256
  I2C1->CR2=(1<<16) | (EEADDR<<1) | I2C_CR2_RD_WRN;
#elif defined M24L08
  I2C1->CR2=(1<<16) | ((EEADDR|block)<<1) | I2C_CR2_RD_WRN;
#endif
  
  I2C1->CR2|=I2C_CR2_START;
  I2C1->CR2|=I2C_CR2_AUTOEND;

  while(!((I2C1->ISR&I2C_ISR_RXNE)==(I2C_ISR_RXNE))); /* TODO: add timeout? */
  *v=I2C1->RXDR;
  
  return 0; /* TODO: add error checking otherwise locks */
}

void ee_save(void *var, uint8_t len, uint16_t ee_base_addr){
  uint8_t i;

  for(i=0; i<len; i++){
    ext_eeprom_wb(ee_base_addr+i, *((uint8_t *)var+i));
    delay_ms(6);
  }
}

void ee_load(void *var, uint8_t len, uint16_t ee_base_addr){
  uint8_t i;
  
  for(i=0; i<len; i++)
    ext_eeprom_rb(ee_base_addr+i, ((uint8_t *)var+i));
}

/* void eeprom_default_check_and_fill(uint8_t force){ */
/*   uint8_t v; */
/*   /\* uint16_t v16; *\/ */
  
/*   /\* ee_hw_major_ver *\/ */
/*   ext_eeprom_rb(ee_hw_major_ver, &v); */
/*   if(v==0xff || force==1){ */
/*     ext_eeprom_wb(ee_hw_major_ver, 0); */
/*     delay_ms(6); */
/*   } */
  
/* } */
