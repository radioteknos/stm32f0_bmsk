/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include "stm32f0xx.h"
#include <stdint.h>
#include <stdlib.h>
#include "main.h"
#include "uart.h"
#include "i2c.h"

extern char buff[BUFFLEN+1];
extern volatile uint32_t systick_to0_cnt;

void i2c0_init(void){

  /* power up i2c */
  RCC->APB1ENR|=RCC_APB1ENR_I2C1EN;

  /* GPIOF AF set*/
  /* SDA (PF0) */
  GPIOF->MODER&=~(0x3<<(0));
  GPIOF->MODER|=(0x2<<(0));
  GPIOF->AFR[0]&=~0xf;
  GPIOF->AFR[0]|=0x1;
  /* SCL (PF1) */
  GPIOF->MODER&=~(0x3<<(1*2));
  GPIOF->MODER|=(0x2<<(1*2));
  GPIOF->AFR[0]&=~0xf0;
  GPIOF->AFR[0]|=(0x1<<4);  
  /* open drain */
  GPIOF->OTYPER|=(1<<0) | (1<<1);

  /* by default I2C1 is clocked by HSI, 8MHz, since I2C1SW in RCC_CFGR3 is 0 */
  /* PRESC=0x1 ; SCLDEL=0x4 ; SDADEL=0x2 ; SCLL=0x13 ; SCLH=0xf */
  I2C1->TIMINGR = (uint32_t)0x10440f13;
  I2C1->CR1=I2C_CR1_PE;                         /* I2C1 en, TX and RX en */

}
