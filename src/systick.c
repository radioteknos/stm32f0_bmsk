/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include "stm32f0xx.h"
#include "main.h"
#include "systick.h"

volatile uint32_t delay_ms_ticks=0, systick_to0_cnt=0;
extern volatile uint32_t stat;

static const uint8_t heartbeat_led_pattern[16]={
  0b10000000, /* normal */
  0b10101010, /* alarm */
  0b01010101, /* alarm (inv) */
  0b00000000, /* off */
  0b11111111, /* on */
  0b10100000, /*  tbd1 */
  0b11111100,  /* tbd2 */
  0b10100010,  /* tbd3 */
  0b00001000, /* inv normal */
  /* TBD */
  0b10101010, /* alarm */
  0b01010101, /* alarm (inv) */
  0b00000000, /* off */
  0b11111111, /* on */
  0b10100000, /* tbd1 */
  0b11111100, /* tbd2 */
  0b10100010   /* tbd3 */
};

uint32_t systick_init(uint32_t ticks){

  if((ticks - 1)>SysTick_LOAD_RELOAD_Msk)
    return 1;      /* reload value impossible */

  /* set reload register */
  SysTick->LOAD=ticks-1;
  
  /* set priority for systick int */
  NVIC_SetPriority(SysTick_IRQn, (1<<__NVIC_PRIO_BITS)-1);

  /* load the systick counter val */
  SysTick->VAL=0;
  
  /* Enable SysTick IRQ and SysTick Timer */
  SysTick->CTRL=SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk |
    SysTick_CTRL_ENABLE_Msk;
  
  return 0;
}

void hbledmode_set(uint8_t mode){
    stat&=~(0xf<<hbledA);
    stat|=((mode&0xf)<<hbledA);
}

static void heartbeat_led(void){
  static uint8_t idx=7;
  uint8_t mask, hbs;

  mask=(1<<idx);
  
  hbs=(stat>>hbledA)&0xf;
  if(heartbeat_led_pattern[hbs]&mask){
    hbled_on();
  }
  else{
    hbled_off();
  }
  idx=(idx-1)&0x07;
  
}

#define LED_PORT GPIOA
#define LED_PIN 5

#define TOGGLE(GPIO, PIN) GPIO->ODR ^= (1 << PIN);


uint32_t heartbeat_presc;

void systick_isr(void){
  
  /* = systick timeout0 = */
  if(systick_to0_cnt!=0)
    systick_to0_cnt--;
  /* # systick timeout0 end # */
  
  /* = blocking delay routine counter = */
  delay_ms_ticks++;
  /* # blocking delay routine counter end # */

  /* = heartbeat = */
  if(heartbeat_presc==0){
    heartbeat_led();
    heartbeat_presc=HEARTBEAT_PRESC_VAL;
  }
  else
    heartbeat_presc--;
  /* # heartbeat end # */
  
}


void delay_ms(uint32_t ms){
  uint32_t now=delay_ms_ticks;
  while((delay_ms_ticks-now)<ms);
}
