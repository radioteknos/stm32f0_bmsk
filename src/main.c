/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include <string.h>
#include "stm32f0xx.h"
#include "systick.h"
#include "uart.h"
#include "misc.h"
#include "callback.h"
#include "i2c.h"
#include "adc.h"
#include "timer.h"
#include "main.h"


volatile uint32_t stat;
extern uint32_t heartbeat_presc;

extern volatile uint8_t usart1_rxbuff[RXBUFFLEN];
extern volatile uint32_t usart1_stat;
extern volatile uint8_t usart1_rxwp, usart1_rxrp, usart1_msgn;

char buff[BUFFLEN+1];

struct CMDS{
  char name[9];
  uint32_t (*funct)(char * params);
};

const struct CMDS cmds[] ={

  /* cmdlen=7 */
  {"getstat", cmd_getstat},
    
  /* cmdlen=6 */
  {"hbmode", cmd_hbmode_set},

  /* cmdlen=3 */
  {"eew", cmd_eeprom_wb},
  {"eer", cmd_eeprom_rb},
  {"adc", cmd_adc_get},
  {"pwm", cmd_pwm_set},
  
  /* cmdlen=2 */
  {"ti", cmd_inttemp_get},

  /* cmdlen=1 */
};


int main(void) {
  char *pb, params[16], cmd[16],msgbuff[32];
  uint32_t i, rv;

  
  /* = system init = */
  system_init();
  /* # system init end # */


  /* = variable init = */
  stat=0x00000000;
  usart1_stat=(1<<uartecho);
  /* # variable init end # */

  
  /* = periferial power up = */  
  /* GPIO A, B, F power up */
  RCC->AHBENR|=(RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOFEN);
  /* # periferial power up end # */  

  
  /* = systick init = */
  /* heartbeat led init */
  HBLED_PORT->MODER|=(1<<(HBLED_PIN*2));
  hbledmode_set(0);

  heartbeat_presc=HEARTBEAT_PRESC_VAL;
  systick_init(sys_clk/1000);
  /* # systick init end # */

  
  /* = periferial init = */
  usart1_init(115200);
  i2c0_init();
  adc_init();
  tim3_init(sys_clk/1000000, 100); /* presc to 1MHz tim3 clk, 100us rate */
  pwm_tim14_init();
  /* # periferial init end # */


  /* ready to go! */
  usart1_puts("\r\nstm32f042_base\r\n");
  usart1_send_prompt(); 

  /* = main loop = */
  while(1){
    if(usart1_msgn>0){
      usart1_get_next_msg(msgbuff);

      /* flush off spaces and tabs */
      pb=(char*)msgbuff; skipspace(&pb);
      
      /* remove trailing \r \n */
      for(i=0; i<strlen(pb); i++)
	if(pb[i]=='\n' || pb[i]=='\r'){
	  pb[i]='\0';
	  break;
	}
      
      if(strlen(pb)!=0){
	
	/* = scan cmds = */
	strncpy(cmd, pb, sizeof(cmd)/sizeof(char)-1);
	for(i=0; i<strlen(cmd); i++)
	  if(cmd[i]==' '){
	    cmd[i]='\0';
	    break;
	  }
	
	for(i=0; i<sizeof(cmds)/sizeof(struct CMDS); i++){
	  if(strcmp(cmd, cmds[i].name)==0){
	    if(strlen(pb)==strlen(cmds[i].name))
	      params[0]='\0';
	    else
	      strncpy(params, pb+strlen(cmds[i].name)+1, 16-1); /* TODO: label */
	    
	    rv=cmds[i].funct(params);
	    
	    if(rv!=0){
	      usart1_puts("\r\n! error !\r\n");	  
	    }
	    break;
	  }
	}
	if(i==sizeof(cmds)/sizeof(struct CMDS)){
	  usart1_send_crlf();
	  usart1_puts(pb);
	  usart1_send_error();
	}
      } /* # scan cmds end # */
      
      usart1_send_prompt(); 
      /* if strlen(pb)==0 the line is empty or just a <CR><LF> has been
	 received, so only a new prompt is send to show that uC is alive */
    }
    
    
  } /* # main loop end # */

}

