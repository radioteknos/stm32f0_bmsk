/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "stm32f0xx.h"
#include "uart.h"
#include "main.h"
#include "misc.h"
#include "systick.h"
#include "eeprom.h"
#include "adc.h"
#include "callback.h"

extern uint32_t sys_clk;
extern volatile uint32_t usart1_stat;
extern char buff[BUFFLEN+1];
extern volatile uint32_t stat;
extern uint32_t tim3_evnt;

extern char buff[BUFFLEN+1];


/* get board status (first implemented command to check if board is running) */
uint32_t cmd_getstat(char* params __attribute__((unused))){

  usart1_puts("\r\n\nsystem core clock ");
  itoa(sys_clk, buff, 10); usart1_puts(buff);  

  usart1_puts("\r\nVREFINT_CAL ");
  itoa(*VREFINT_CAL_ADDR, buff, 10); usart1_puts(buff);  
  usart1_puts("\r\nTS_CAL1(30^C) ");
  itoa(*TS_CAL1, buff, 10); usart1_puts(buff);  
  usart1_puts("\r\nTS_CAL2(110^C) ");
  itoa(*TS_CAL2, buff, 10); usart1_puts(buff);  
  usart1_send_crlf();
  
  usart1_puts("\r\ntim3_evnt ");
  itoa(tim3_evnt, buff, 10); usart1_puts(buff);  
  usart1_send_crlf();
  
  return 0;
}

uint32_t cmd_hbmode_set(char* params){
  uint8_t v;
  char *pb;

  pb=params; skipspace(&pb);
  v=strtol(pb, NULL, 16);

  hbledmode_set(v);
  
  return 0;
}

uint32_t cmd_eeprom_wb(char* params){
  uint16_t addr, rv;
  uint8_t data;
  char *pb;

  pb=params; skipspace(&pb);
  addr=strtol(pb, NULL, 16);
  skipnotspace(&pb); skipspace(&pb);
  data=strtol(pb, NULL, 16);

  rv=ext_eeprom_wb(addr, data);
  itoa(rv, buff, 16); usart1_puts(buff); usart1_send_crlf();
  
  return 0;
}

uint32_t cmd_eeprom_rb(char* params){
  uint16_t addr, rv;
  uint8_t data;
  char *pb;

  pb=params; skipspace(&pb);
  addr=strtol(pb, NULL, 16);

  rv=ext_eeprom_rb(addr, &data);
  
  usart1_send_crlf();
  itoa(data, buff, 16); usart1_puts(buff);
  usart1_puts("\t'"); usart1_putc(data); usart1_puts("\'\t(");
  itoa(rv, buff, 16); usart1_puts(buff); usart1_puts(")\n");
  
  return 0;
}

uint32_t cmd_adc_get(char* params){
  uint16_t v;
  uint8_t ch;
  char *pb;

  pb=params; skipspace(&pb);
  ch=strtol(pb, NULL, 10);

  v=adc_get(ch);

  if(v==0xfffe){
    usart1_puts("\r\nwrong ch!\r\n");
    return 1;
  }
  else if(v==0xffff){
    usart1_puts("\r\nadc disabled!\r\n");
    return 1;
  }    
  else{
    usart1_send_crlf();
    itoa(v, buff, 10);
    usart1_puts(buff);
    usart1_puts("\t0x");
    itoa(v, buff, 16);
    usart1_puts(buff);
    usart1_send_crlf();
  }
  
  return 0;
}


uint32_t cmd_inttemp_get(char* params __attribute__((unused))){
  uint16_t v;
  int32_t temp; /* ^C */

  v=adc_get(16);

  temp=(((int32_t)v*VDD_APPLI/VDD_CALIB)-(int32_t)*TEMP30_CAL_ADDR);
  temp=temp*(int32_t)(110-30);
  temp=temp/(int32_t)(*TEMP110_CAL_ADDR-*TEMP30_CAL_ADDR);
  temp=temp+30;
  
  usart1_puts("\r\nintTemp=  ");
  itoa(temp, buff, 10); usart1_puts(buff);  
  usart1_puts("^C\r\n");
  
  return 0;
}

uint32_t cmd_pwm_set(char* params){
  uint8_t v;
  char *pb;

  pb=params; skipspace(&pb);
  v=strtol(pb, NULL, 10);

  if(v>100)
    return 1;

  TIM14->CCR1=v;

  return 0;
}
