/*
*  stm32f0_bmsk
*
*     (c) by Lapo Pieri 2021
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* Send bugs reports, comments, critique, etc, to
*
*       lapo_CHANGE_THIS_AT_CHANGE_THIS_radioteknos.it
*/

#include <stdint.h>
#include <string.h>
#include "stm32f0xx.h"
#include "systick.h"
#include "main.h"
#include "uart.h"

volatile uint8_t usart1_rxbuff[RXBUFFLEN];
volatile uint32_t usart1_stat=0x00000000;
volatile uint8_t usart1_rxwp=0, usart1_rxrp=0, usart1_msgn=0;

void usart1_init(uint32_t baudrate){
  
  /* power up usart1 */
  RCC->APB2ENR|=RCC_APB2ENR_USART1EN;
  
  /* GPIOA AF set*/
  /* TXD */
  GPIOA->MODER&=~(0x3<<(9*2));
  GPIOA->MODER|=(0x2<<(9*2));
  GPIOA->AFR[1]&=~0xf0;
  GPIOA->AFR[1]|=(0x1<<4);
  /* RXD */
  GPIOA->MODER&=~(0x3<<(10*2));
  GPIOA->MODER|=(0x2<<(10*2));
  GPIOA->AFR[1]&=~0xf00;
  GPIOA->AFR[1]|=(0x1<<8);  
  
  /* oversampling x16, baudrate */
  USART1->BRR = sys_clk/baudrate;
  /* N81, TX en, RX en, RX int en, USART1 en */
  USART1->CR1 = USART_CR1_TE | USART_CR1_UE
    | USART_CR1_RXNEIE | USART_CR1_RE;
  
  NVIC_EnableIRQ(USART1_IRQn);
  NVIC_SetPriority(USART1_IRQn, 2);  /* TODO: adj pri */
}

#define LED_PORT GPIOA
#define LED_PIN 5
#define TOGGLE(GPIO, PIN) GPIO->ODR ^= (1 << PIN);

void usart1_isr(void){
  uint8_t rxb;

  if(USART1->ISR&USART_ISR_RXNE){
  
    TOGGLE(LED_PORT, LED_PIN); /* TODO: remove (debug) */
    rxb=USART1->RDR;

    if(rxb==0x08 || rxb==0x7f){                 /* simple backspace */
      usart1_putc('\b');
      usart1_putc(' ');
      usart1_putc('\b');

      if(usart1_rxwp!=usart1_rxrp){
	if(usart1_rxwp>0)
	  usart1_rxwp--;
	else
	  usart1_rxwp=RXBUFFLEN-1;
      }
    }
    else{
      usart1_rxbuff[usart1_rxwp]=rxb;
      if(usart1_stat&(1<<uartecho)){
	usart1_putc(rxb);
      }
      if(rxb=='\n' || rxb=='\r'){  /* if rx'ed byte is a line terminator */
	usart1_msgn++;      
      }

      usart1_rxwp++;
      if(usart1_rxwp>RXBUFFLEN-1)
	usart1_rxwp=0;
      if(usart1_rxwp==usart1_rxrp){
	usart1_putc('\a');
	usart1_rxwp=0; /* TODO: better (save the queue!) */
	usart1_rxrp=0;
	usart1_rxbuff[usart1_rxwp]='\0';
	usart1_msgn=0;	
      }

    }
      
  }

  /* TODO: else if(...other int (PE, TXE...)...) */
  
}



int usart1_get_next_msg(char *b){
  int i=0;
  char t;

  if(usart1_msgn==0)
    return 0;
  else{
    while(usart1_rxrp!=usart1_rxwp){
      t=usart1_rxbuff[usart1_rxrp];
      b[i]=t;
      usart1_rxrp++;
      if(usart1_rxrp>RXBUFFLEN-1)
	usart1_rxrp=0;
      i++;
      if(t=='\r' || t=='\n'){
	b[i]='\0';
	break;
      }
    }
    if(b[i]!='\0')
      return -1;
    usart1_msgn--;
    return 1;
  }
}


void usart1_putc(char c){
  uint8_t txl=1;
  
  while(txl){
    if(USART1->ISR&USART_ISR_TXE){
      USART1->TDR=c;
      txl=0;
    }
    else{
      /* TODO: add timeout */
    }
  }  
}


void usart1_puts(char* b){
  while(*b!='\0'){
    usart1_putc(*b);
    b++;
  }
}
